��    n      �  �   �      P	     Q	     W	     `	     l	     {	     �	     �	     �	  (   �	     �	     �	     �	     �	     �	     
     
     0
     4
     ;
  	   O
     Y
     n
     �
     �
  	   �
     �
     �
     �
  #   �
     �
                    -     1  	   8     B     O  	   c     m     ~     �     �     �     �     �     �     �     �      �     �     �     �     �     �     �               4     D     \     k     �     �     �     �     �     �     �     �     �                ,     5     G     U     d     �     �     �     �     �     �     �     �     �  	   �                    -     1     8  
   K     V     Z  	   c     m     q     y     �  	   �     �     �     �     �     �     �    �     �     �     �     �                )     +  3   4     h     y     �     �     �     �     �     �     �     �     	          5     G     Y  
   w     �     �     �  9   �     �  	   �     �          $     (     4     @     N     a     o     �     �     �     �     �     �     �     �     �  "   �     �     �     �     �               %     7     P      j     �  &   �     �     �     �          
            #        C     [      u  
   �     �     �     �  $   �     �          +     0  "   ?     b     f     m     �     �     �     �     �     �     �     �  	   �     �     �     �          	          (     ,     9     F     _     e     m     r        (              ]      ?   -   i       a   A       =      .       K   j   E   >       Q       ;   ^   +       X       
   "                               #   f             5      b              M   *   g       d                V   	      '   0         h      ,           9   n   :   D   6   3              %   `   U                 [   )   <       W   l      c   \   F       8   /       $       O      C   S               R   1          2   k      m   J   G                  H   N       T   e   Y   &   Z   4   !       @      P   B   7       _   L           I    %H:%M %H:%M:%S %I:%M:%S %p %a %b %e %T %Y %a %b %e %T %Y %Z %m/%d/%y . Aborted Address family not supported by protocol Address in use Address not available Alarm clock Apr April Argument list too long Arithmetic exception Aug August Bad file descriptor Bus error Child process status Connection aborted Connection refused Connection reset by network Continued Dec December Directory not empty Error loading shared library %s: %m Feb February File too large Filename too long Fri Friday I/O error I/O possible Illegal instruction Interrupt Invalid argument Invalid regexp Is a directory Jan January Jul July Jun June Killed Library %s is not already loaded Mar March May Mon Monday Network is down Network unreachable No error information No medium found No space left on device No such device No such file or directory No such process Not a directory Not supported Nov November Oct October Operation already in progress Operation in progress Operation not permitted Operation timed out Overflow Permission denied Power failure Protocol error Protocol family not supported Protocol not available Protocol not supported Quit Quota exceeded Read-only file system Sat Saturday Segmentation fault Sep September Stack fault Stopped Stopped (signal) Sun Sunday Symbolic link loop Terminated Thu Thursday Try again Tue Tuesday Unknown error Wed Wednesday Window changed Wrong medium type ^[nN] ^[yY] no yes Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n > 1);
Language: pt_BR
 %H:%M %H:%M:%S %I:%M:%S %p %A, %e de %B de %Y %T %A, %e de %B de %Y %T %Z %d/%m/%y , Abortado Família de endereço não suportada pelo protocolo Endereço em uso Endereço não disponível Despertador Abr Abril Lista de argumento muito longa Exceção Arritmética Ago Agosto Falha no descritor de arquivo Erro de Barramento Status do processo filho Conexão abortada Conexão recusada Conexão redefinida pela rede Continuado Dez Dezembro Diretório não está vazio Erro ao carregar biblioteca compartilhada (shared) %s: %m Fev Fevereiro Arquivo muito grande Nome de arquivo muito longo Sex Sexta-feira Erro de E/S E/S possível Instrução ilegal Interrupção Argumento inválido Regexp inválido É um diretório Jan Janeiro Jul Julho Jun Junho Morto A Biblioteca %s não foi carregada Mar Março Mai Seg Segunda-feira Rede não disponível Rede inacessível Sem informação do erro Nenhuma mídia encontrada Sem espaço livre no dispositivo Dispositivo desconhecido Arquivo ou diretório não encontrados Processo desconhecido Não é um diretório Não suportado Nov Novembro Out Outubro A operação já está em progresso Operação em progresso Operação não permitida Tempo esgotado para a operação Sobrecarga Acesso negado Falha de energia Erro de protocolo Família de protocolo não suportada Protocolo não disponível Protocolo não suportado Sair Quota excedida Sistema de arquivo somente leitura Sab Sabado Falha de segmentação Set Setembro Falha de pilha Parado Parado (sinal) Dom Domingo Loop de link simbólico Encerrado Qui Quinta-feira Tente novamente Ter Terça-feira Erro desconhecido Qua Quarta-feira Janela mudou Tipo de mídia incorreta ^[nN] ^[sSyY] não sim 